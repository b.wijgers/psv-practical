module Parser.Applied
  ( integer
  , decInteger, octInteger, hexInteger
  , skipWhitespace
  )
where

import Parser.Core
import Control.Applicative ((<|>))
import Data.Char (isSpace)

skipWhitespace :: Parser Char ()
skipWhitespace = () <$ greedy (satisfy isSpace)

isDecDigit :: Char -> Bool
isDecDigit = flip elem "0123456789"

isOctDigit :: Char -> Bool
isOctDigit = flip elem "01234567"

isHexDigit :: Char -> Bool
isHexDigit = flip elem "0123456789AaBbCcDdEeFf"

integer :: Parser Char Int
integer = hexInteger <<|> octInteger <<|> decInteger

decInteger :: Parser Char Int
decInteger = readDec
          <$> greedy1 (satisfy isDecDigit)

octInteger :: Parser Char Int
octInteger = readOct
          <$  satisfy (=='0')
          <*> greedy1 (satisfy isOctDigit)

hexInteger :: Parser Char Int
hexInteger = readHex
          <$  satisfy (=='0')
          <*  satisfy (\x -> x == 'x' || x == 'X')
          <*> greedy1 (satisfy isHexDigit)

readDec :: String -> Int
readDec = foldl f 0
  where
    f n c = n * 10 + charAsInt c

readOct :: String -> Int
readOct = foldl f 0
  where
    f n c = n * 8 + charAsInt c

readHex :: String -> Int
readHex = foldl f 0
  where
    f n c = n * 16 + charAsInt c

charAsInt :: Char -> Int
charAsInt '0' = 0
charAsInt '1' = 1
charAsInt '2' = 2
charAsInt '3' = 3
charAsInt '4' = 4
charAsInt '5' = 5
charAsInt '6' = 6
charAsInt '7' = 7
charAsInt '8' = 8
charAsInt '9' = 9
charAsInt 'a' = 10
charAsInt 'A' = 10
charAsInt 'b' = 11
charAsInt 'B' = 11
charAsInt 'c' = 12
charAsInt 'C' = 12
charAsInt 'd' = 13
charAsInt 'D' = 13
charAsInt 'e' = 14
charAsInt 'E' = 14
charAsInt 'f' = 15
charAsInt 'F' = 15

