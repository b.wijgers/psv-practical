module Parser.Core
  ( Parser
  , runParser
  , matchAny
  , satisfy
  , failParser
  , many, some, greedy, greedy1
  , option
  , lookahead, lookaheadAll
  , (<<|>)
  )
where

import Control.Applicative (Alternative(..))

data Parser s r = P ([s] -> [(r, [s])])

instance Functor (Parser s) where
  -- fmap f (P p) = P $ \s -> let (r, s') = p s in (f r, s')
  fmap f (P p) = P $ \s -> [(f r, s') | (r, s') <- p s]

instance Applicative (Parser s) where
  pure x = P $ \s -> [(x, s)]
  --(P l) <*> (P r) =
  --  P $ \s -> let (a, s') = l s in let (b, s'') = r s' in (a b, s'')
  (P l) <*> (P r) =
    P $ \s -> [(a b, s'') | (a, s') <- l s, (b, s'') <- r s']

instance Monad (Parser s) where
  return = pure
  -- (>>=) :: Parser s a -> (a -> Parser s b) -> Parser s b
  (P p) >>= f =
    P $ \s -> concat [let (P r) = f a in r s' | (a, s') <- p s]

instance Alternative (Parser s) where
  empty = failParser
  (P l) <|> (P r) = P $ \s -> l s ++ r s
  many = pMany
  some = pSome

pSome :: Parser a b -> Parser a [b]
--pSome p = (:) <$> p <*> many p
pSome p = do
  x <- p
  rest <- many p
  return $ x : rest

pMany :: Parser a b -> Parser a [b]
pMany p = pSome p <|> return []

greedy1 :: Parser a b -> Parser a [b]
greedy1 p = do
  x <- p
  rest <- greedy p
  return $ x : rest

greedy :: Parser a b -> Parser a [b]
greedy p = greedy1 p <<|> return []

(<<|>) :: Parser a b -> Parser a b -> Parser a b
(P l) <<|> (P r) = P f
  where
    f s = let xs = l s in case xs of
      [] -> r s
      _  -> xs

lookahead :: Int -> Parser a [a]
lookahead n = P f
  where
    f [] = []
    f xs = [(take n xs, xs)]

lookaheadAll :: Parser a [a]
lookaheadAll = P f
  where
    f [] = []
    f xs = [(xs, xs)]

matchAny :: Parser a a
matchAny = P f
  where
    f []      = []
    f (x:xs)  = [(x, xs)]

failParser :: Parser a b
failParser = P $ \_ -> []

option :: Parser s a -> Parser s (Maybe a)
option p = P $ \s -> [(Nothing, s)] ++ ((\(r, s') -> (Just r, s')) <$> runParser p s)

satisfy :: Eq s => (s -> Bool) -> Parser s s
satisfy p = do
  x <- matchAny
  if p x
    then return x
    else failParser

runParser :: Parser a r -> [a] -> [(r, [a])]
runParser (P p) s = p s

