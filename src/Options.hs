module Options 
  (
    Options (Options)
    , oVerifierMode
    , oFilePathOfProgram
    , oMaxPathLength
    , oExperimentParameter
    , oEnableDebug
    , oTreeTrimmerMode
    , parseOptions
  )
where

import Data.Semigroup ((<>))
import Options.Applicative
import WLP.TreeTrimmer as TreeTrimmerMode (TreeTrimmerMode(..))

-- \x -> x `seq` x

data Options = Options { 
    oFilePathOfProgram :: String,
    oMaxPathLength :: Int,
    oExperimentParameter :: Int,
    oTreeTrimmerMode :: TreeTrimmerMode,
    oEnableDebug :: Bool,
    oVerifierMode :: Bool
}

options :: Parser Options
options = Options <$>
          strOption ( long "input" 
                      <> short 'i' 
                      <> metavar "FILEPATH" 
                      <> help "File path of the program that will be verified."
                      <> value "examples/id"
                      <> showDefault)
          <*> option auto (long "maxPathLength"
                      <> short 'k'
                      <> metavar "INTEGER" 
                      <> help "Maximum path length."
                      <> value 20
                      <> showDefault)
          <*> option auto (long "experimentParameter"
                      <> short 'N'
                      <> metavar "INTEGER"
                      <> help "The experiment parameter for the benchmarks."
                      <> value 1
                      <> showDefault)
          <*> option auto ( long "treeTrimmingPolicy"
                      <> short 't'
                      <> help "The tree trimming policy, i.e. in which cases the tree should be trimmed."
                      <> value TreeTrimmerMode.Always
                      <> showDefault)
          <*> switch (long "debug"
                      <> short 'd'
                      <> help "Run the verifier in debug mode")
          <*> switch (long "verifier"
                      <> short 'v'
                      <> help "Run the verfier without benchmarks")


parseOptions :: IO Options
parseOptions = execParser opts
  where
      opts = info (options <**> helper)
          ( fullDesc 
          <> progDesc "This verifier generates a WLP according to the provided program and uses z3 for verification" 
          <> header "ProgVer - The greatest program verfifier to ever walked the face of this planet." )
