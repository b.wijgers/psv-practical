{-# LANGUAGE OverloadedStrings #-}

module Main where

import Options
import Verifier
import System.CPUTime
import Text.Printf
import Criterion.Main
import WLP.TreeTrimmer as TreeTrimmerMode (TreeTrimmerMode(..))
import Data.Text
import Criterion.Types

main :: IO ()
main = do
  options <- parseOptions
  let verifierMode = oVerifierMode options
  if verifierMode 
    then runNormal options
    else defaultMainWith config [bgroup "benchmarks" benchmarks] 

generateBenchmarks :: String -> [TreeTrimmerMode] -> [Int] -> [Int] -> [Benchmark]
generateBenchmarks f tms ns ps = [generateBenchmark tm' n' p' | n' <- ns, p' <- ps, tm' <- tms]
  where
    generateBenchmark :: TreeTrimmerMode -> Int -> Int -> Benchmark
    generateBenchmark tm n p = bench (benchmarkName f tm n p) $ whnfIO $ runVerifier $ generateBenchmarkOption tm n p

    generateBenchmarkOption :: TreeTrimmerMode -> Int -> Int -> Options
    generateBenchmarkOption tm n p = Options { 
      oFilePathOfProgram = "./benchmarks/" ++ f,
      oMaxPathLength = p,
      oExperimentParameter = n,
      oTreeTrimmerMode = tm,
      oEnableDebug = False,
      oVerifierMode = False
  }

config :: Config
config = defaultConfig {
  csvFile = Just "./results/benchmarks.csv",
  reportFile = Just "./results/report.html",
  rawDataFile = Just "./results/rawData"
}

benchmarkName :: String -> TreeTrimmerMode -> Int -> Int -> String
benchmarkName f tm n p = f ++ "/" ++ show tm ++ "/N:" ++ show n ++ "/P:" ++ show p

benchmarks :: [Benchmark]
benchmarks = divByN ++ memberOf ++ pullUp
    where
      divByN = generateBenchmarks "divByN" tms ns ps 
      memberOf = generateBenchmarks "memberOf" tms ns ps 
      pullUp = generateBenchmarks "pullUp" tms ns ps 
      tms = [Always, Smart, Never]
      ns = [2..10]
      ps = [10,20..30]
  
runNormal :: Options -> IO ()
runNormal o = do
  start <- getCPUTime
  runVerifier o
  end   <- getCPUTime
  let diff = (fromIntegral (end - start)) / (10^12)
  printf "Computation time: %0.3f sec\n" (diff :: Double)