{-# LANGUAGE OverloadedStrings, CPP, BangPatterns #-}

module Verifier
  (
    runVerifier
  )
where

import GCLParser.Parser
import GCLParser.GCLDatatype (Expr(LitI))
import GCLParser.GCLAlgebra
import GCLParser.GCLDatatype

import GCLLexer.Lexer

import WLP.LogicalExpression
import WLP.ProgramPath
import WLP.ProgramPathTree
import WLP.TreeTrimmer
import WLP.WLPCalculator
import WLP.RenameFunctions (replace)

import Z3.Z3ModelParser
import Z3.Z3Runner
import Z3.Z3Theorem

import Options

import Control.Monad
import System.Environment
import Control.Exception

import System.CPUTime
import Text.Printf

runVerifier :: Options -> IO ()
runVerifier o = do
  file <- readFile $ oFilePathOfProgram o
  let debugMode = oEnableDebug o
  let maxPathLength = oMaxPathLength o
  let experimentParameter = oExperimentParameter o
  let treeTrimmerMode = oTreeTrimmerMode o

  let tokens = lexer file
  when (debugMode) $ print' "Tokens" tokens

  -- Convert Tokens to Expression
  let parsed = parseGCL tokens
  case parsed of
    -- Parsing error has occured
    Left xs -> putStrLn "Could not parse program. Error:" >> print xs
    Right program -> do
      when (debugMode) $ print' "Parsed" program

      -- Get the variable declarations from the program
      let variableDeclarations = getVariablesWithTypes program
      when (debugMode) $ print' "Variable declarations" variableDeclarations

      -- Convert Program to ProgramPathTree
      let tree = toProgramPathTree program maxPathLength
      when (debugMode) $ print' "Path Tree" tree

      -- Trim the tree
      start <- getCPUTime
      trimmedTree <- trimIfs treeTrimmerMode tree variableDeclarations experimentParameter
      end <- getCPUTime
      let diff = (fromIntegral (end - start)) / (10^12)
      printf "Tree trimming time: %0.3f sec\n" (diff :: Double)

      when (debugMode) $ print' "Trimmed Tree" trimmedTree

      -- Convert Expression to ProgramPaths
      let paths = getProgramPaths maxPathLength trimmedTree
      when (debugMode) $ print' "Paths" paths

      -- Normalize the ProgramPaths
      let normalizedPaths = normalizeProgramPath <$> paths
      when (debugMode) $ print' "Normalized Paths" normalizedPaths

      -- Convert ProgramPaths to WLP
      let wlps = replace "N" (LitI experimentParameter) . programPathToWLP <$> normalizedPaths

      -- Print Results
      let models = verify debugMode variableDeclarations wlps

      start <- getCPUTime
      res <- runIOUntil isInvalid print (putStrLn "Program is valid!") models 0
      end <- getCPUTime
      let diff = (fromIntegral (end - start)) / (10^12)
      printf "Z3 verifying time: %0.3f sec\n" (diff :: Double)
      printf "Total atoms in WLPs: %i\n" (snd res)
      return $! fst res

-- Last int argument is the current total of atoms in the WLPs found
runIOUntil :: (a -> Bool) -> (a -> IO b) -> IO b -> [IO (a, Int)] -> Int -> IO (b, Int)
runIOUntil _ _ d []     c = (,) <$> d <*> pure c
runIOUntil p t d (x:xs) c = do
  (x', c') <- x
  let count = c + c'
  if p x'
    then (,) <$> t x' <*> pure count
    else runIOUntil p t d xs count

verify :: Bool -> [VarDeclaration] -> [LogicalExpression] -> [IO (SatModel, Int)]
verify debugMode vars wlps = map verify' wlps
      where
        verify' :: LogicalExpression -> IO (SatModel, Int)
        verify' wlp  = do
          when (debugMode) $ print "------------------------------------"
          when (debugMode) $ print' "WLP" wlp

          -- Reduce WLP
          let reducedWlp = simplify $ reduceExpressions wlp
          when (debugMode) $ print' "Reduced WLP" reducedWlp

          -- Count atoms
          let atomCount = leSize reducedWlp
          when (debugMode) $ print' "Atom count" atomCount

          -- Transform WLP to Z3 Theorem
          let output = transformZ3 reducedWlp vars
          when (debugMode) $ print' "Z3 Theorem" output

          -- Run z3 verifier
          result <- runZ3 output debugMode

          -- Parse result to see if this is satisfiable
          return $ (parseSatModel result, atomCount)


print' :: Show a => String -> a -> IO ()
print' title value = do
  putStrLn $ title ++ ": "
  print value
