module WLP.ProgramPathTree
  ( toProgramPathTree
  , ProgramPathTree(Node, IfNode, Leaf)
  )
where

import GCLParser.GCLAlgebra
import GCLParser.GCLDatatype

data ProgramPathTree  = Node Stmt ProgramPathTree
                      | IfNode Expr ProgramPathTree ProgramPathTree
                      | Leaf
                      deriving (Show)

toProgramPathTree :: Program -> Int -> ProgramPathTree
toProgramPathTree = foldProgram toPPTAlgebra

toPPTAlgebra :: ProgramAlgebra PrimitiveType Type VarDeclaration (Int -> Int -> Maybe (ProgramPathTree, Int)) Expr BinOp (Int -> ProgramPathTree)
toPPTAlgebra =
  ( program
  , variableDeclaration
  , ( ptInt
    , ptBool
    )
  , ( ptype
    , reftype
    , arraytype
    )
  , ( skip
    , assert
    , assume
    , assign
    , aassign
    , drefassign
    , seq
    , ifthenelse
    , while
    , block
    , trycatch
    )
  , ( Var
    , LitI
    , LitB
    , LitNull
    , Parens
    , ArrayElem
    , OpNeg
    , BinopExpr
    , Forall
    , SizeOf
    , RepBy
    , Cond
    , NewStore
    , Dereference
    )
  , ( And
    , Or
    , Implication
    , LessThan
    , LessThanEqual
    , GreaterThan
    , GreaterThanEqual
    , Equal
    , Minus
    , Plus
    , Multiply
    , Divide
    , Alias
    )
  )
  where
    program _ _ _ stmt  = \m -> maybe Leaf fst (stmt m 0)
    variableDeclaration = undefined
    ptInt               = undefined
    ptBool              = undefined
    ptype               = undefined
    reftype             = undefined
    arraytype           = undefined
    skip                = \m c -> if m >= c then Just (Node Skip Leaf, c + 1) else Nothing
    assert me           = \m c -> if m >= c then Just (Node (Assert me) Leaf, c + 1) else Nothing
    assume me           = \m c -> if m >= c then Just (Node (Assume me) Leaf, c + 1) else Nothing
    assign var e        = \m c -> if m >= c then Just (Node (Assign var e) Leaf, c + 1) else Nothing
    aassign var i v     = \m c -> if m >= c then Just (Node (AAssign var i v) Leaf, c + 1) else Nothing
    drefassign var v    = \m c -> if m >= c then Just (Node (DrefAssign var v) Leaf, c + 1) else Nothing
    --ifthenelse g s1 s2  = \m c -> IfNode g (s1 i) (s2 i)
    --while g s           = \m c -> case addToEnd i (s i) (while g s i) of -- Problem is that this while still infinitely recurses
    --                        Just s' -> IfNode g s' Leaf
    --                        _       -> Leaf
    --block decls s       = \i -> s i
    ifthenelse = undefined
    while = undefined
    block = undefined
    trycatch            = undefined -- We don't do that here
    
    seq :: (Int -> Int -> Maybe (ProgramPathTree, Int)) -> (Int -> Int -> Maybe (ProgramPathTree, Int)) -> (Int -> Int -> Maybe (ProgramPathTree, Int))
    seq s1 s2 = \m c -> do
      s1' <- s1 m c
      let (n1, l1) = s1'
      s2' <- s1 m l1
      let (n2, l2) = s2'
      return $ (addToEnd n1 n2, l2)

    -- seq s1 s2 = \i -> seq' i (s1 i) (s2 i)  -- Fix is by making the statements here see that they don't have as much length as originally thought
    --                                         -- We can reduce i based on how many seqs we've seen, and stop recursing here when there's another seq 
    --                                         -- If the while still infinitely recurses, just add a -1 to all if-nodes as well or something, whatever
    seq' i (Node p1 Leaf) (Node p2 n2)         = Node p1 $ Node p2 n2                                                                              
    seq' i (Node p1 n1) n2@(Node _ _)          = case addToEnd i n1 n2 of
                                                Just n' -> Node p1 n'
                                                _       -> Leaf

    seq' i (IfNode g pt pe) n@(Node _ _)       = case addToEnd i pt n of
                                                Just t' -> case addToEnd i pe n of
                                                  Just e' -> IfNode g t' e'
                                                  _       -> t'
                                                _       -> case addToEnd i pe n of
                                                  Just e' -> e'
                                                  _       -> Leaf
    seq' i (Node s Leaf) n@(IfNode _ _ _)      = Node s n

    seq' i s1 s2 = error $  "Non exhaustive patterns in seq, file WLP/ProgramPathTree, line ~90.\n"
                        ++ "No pattern exists for following arguments:\n\t"
                        ++ show s1 ++ "\n\t" ++ show s2

    addToEnd :: ProgramPathTree -> ProgramPathTree -> ProgramPathTree
    addToEnd = ate'

    ate' Leaf n                 = n
    ate' (Node s Leaf) n        = Node s n
    ate' (Node s n1) n2         = case addToEnd (c + 1) n1 n2 of
                                    Just n' -> if c < m then Just $ Node s n' else Nothing
                                    _       -> Nothing
    ate' n1 n2                  = error $ "No pattern for arguments:\n\t" ++ show n1 ++ "\n\t" ++ show n2

