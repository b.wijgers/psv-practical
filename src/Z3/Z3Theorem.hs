module Z3.Z3Theorem
  (
    transformZ3
    , Z3Theorem
  )
where

import WLP.LogicalExpression
import GCLParser.GCLDatatype
import Data.Char (toLower)
import qualified Data.Map as Map

type Z3Theorem = String

transformZ3 :: LogicalExpression -> [VarDeclaration] -> Z3Theorem
transformZ3 lexpr vars = consts ++ assert ++ checkModel ++ getModel
  where
    consts = transformVariables vars
    assert = "(assert " ++ transformAssert (Not lexpr) ++ ")"
    checkModel = "(check-sat)"
    getModel = "(get-model)"

transformVariables :: [VarDeclaration] -> Z3Theorem
transformVariables = foldr (\v z3 -> z3 ++ transformVariable v) ""
    where
      transformVariable :: VarDeclaration -> Z3Theorem
      transformVariable (VarDeclaration x (PType PTInt)) = "(declare-const " ++ x ++ " Int)"
      transformVariable (VarDeclaration x (PType PTBool)) = "(declare-const " ++ x ++ " Bool)"
      transformVariable (VarDeclaration x (AType PTInt)) = "(declare-const " ++ x ++ " (Array Int Int))(declare-const " ++ x ++ "_length Int)"
      transformVariable (VarDeclaration x (AType PTBool)) = "(declare-const " ++ x ++ " (Array Int Bool))"
      transformVariable _ = undefined

transformAssert :: LogicalExpression -> Z3Theorem
transformAssert (Constant c) = map toLower $ show c
transformAssert (Variable x) = x
transformAssert (Not lexpr) = "(not " ++ transformAssert lexpr ++ ")" 
transformAssert (lexpr1 :/\: lexpr2) = "(and " ++ transformAssert lexpr1 ++ " " ++ transformAssert lexpr2 ++ ")"  
transformAssert (lexpr1 :\/: lexpr2) = "(or " ++ transformAssert lexpr1 ++ " " ++ transformAssert lexpr2 ++ ")" 
transformAssert (lexpr1 :=>: lexpr2) = "(=> " ++ transformAssert lexpr1 ++ " " ++ transformAssert lexpr2 ++ ")"
transformAssert (ForAll x lexpr)     = "(forall ((" ++ x ++ " Int))" ++ transformAssert lexpr ++ ")"
transformAssert (Expression expr)    = transformAssert' expr
      where
        transformAssert' :: Expr -> Z3Theorem
        transformAssert' (Var x) = x
        transformAssert' (LitI n) = show n
        transformAssert' (LitB b) = map toLower $ show b
        transformAssert' (LitNull) = undefined
        transformAssert' (Parens expr) = transformAssert' expr
        transformAssert' (ArrayElem array i) = "(select " ++ transformAssert' array ++ " " ++ transformAssert' i ++ ")"
        transformAssert' (OpNeg expr) = "(not " ++ transformAssert' expr ++ ")"
        transformAssert' (BinopExpr op expr1 expr2) = "(" ++ show op ++ " " ++ transformAssert' expr1 ++ " " ++ transformAssert' expr2 ++ ")"
        transformAssert' (Forall x expr) = "(forall ((" ++ x ++ " Int))" ++ transformAssert' expr ++ ")"
        transformAssert' (SizeOf e) = getArrayName e ++ "_length"
        transformAssert' (RepBy a i v) = "(store " ++ transformAssert' a ++ " " ++ transformAssert' i ++ " " ++ transformAssert' v  ++ ")"
        transformAssert' (Cond cond b1 b2) =  "(ite (" ++ transformAssert' cond ++ ") " ++ transformAssert' b1 ++ " " ++ transformAssert' b2 ++ ")"
        transformAssert' (NewStore _) = undefined
        transformAssert' (Dereference _) = undefined

        getArrayName (Var x) = x
        getArrayName (RepBy a _ _) = getArrayName a
transformAssert le = error $ "Called transformAssert with invalid value: \n\t" ++ show le

