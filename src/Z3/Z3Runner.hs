{-# LANGUAGE OverloadedStrings, CPP #-}

module Z3.Z3Runner
  (
    runZ3
  )
where

import Z3.Z3Theorem

import Shelly
import Data.Text
import Prelude hiding (FilePath)

runZ3 :: Z3Theorem -> Bool -> IO String
runZ3 theorem debugMode = do
    output <- shelly $ errExit False $ print_stdout debugMode $ runZ3'
    return $ unpack output
  where
    runZ3' :: Sh Text
    runZ3' = do
      let z3 = command "z3" ["-smt2"]
      let theorem' = pack theorem
      setStdin theorem' >> z3 ["-in"]
