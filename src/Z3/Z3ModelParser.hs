module Z3.Z3ModelParser
  ( parseSatModel
  , isInvalid
  , SatModel(Valid, Invalid)
  )
where

import Data.Char (isSpace)

import Parser.Core
import Parser.Applied
import Control.Applicative
import qualified Data.Map as Map

type Variable = String
type Value = String
data SatModel = Valid
              | Invalid String

instance Show SatModel where
  show Valid        = "Valid program path!"
  show (Invalid xs) = "Program is invalid. Model that does not satisfy pre/postconditions:\n" ++ xs

parseSatModel :: String -> SatModel
parseSatModel z3out = case runParser satParser z3out of
  []            -> Valid
  ((_, xs):_)   -> Invalid xs

isInvalid :: SatModel -> Bool
isInvalid (Invalid _) = True
isInvalid _           = False

satParser :: Parser Char String
satParser = matchString "sat" <* skipWhitespace

satModelParser :: Parser Char (Map.Map Variable Value)
satModelParser = sequence (map (\x -> satisfy (==x)) "sat") *> skipWhitespace *> manyModelParser

manyModelParser :: Parser Char (Map.Map Variable Value)
manyModelParser = Map.fromList <$ matchString "(model" <* skipWhitespace <*> many modelParser <* matchString ")"

modelParser :: Parser Char (String, String)
modelParser =  (,) <$
                  matchString "(define-fun" <* skipWhitespace
              <*> many (satisfy (not . isSpace)) <* skipWhitespace
              <*  matchString "()" <* skipWhitespace
              <*  pType <* skipWhitespace
              <*> many (satisfy (\x -> not (isSpace x) && x /= ')')) <* skipWhitespace
              <*  satisfy (==')') <* skipWhitespace

pType :: Parser Char String
pType = pPType <<|> pAType

pPType :: Parser Char String
pPType = many (satisfy (not . isSpace))

pAType :: Parser Char String
pAType = undefined

matchString :: String -> Parser Char String
matchString s = sequence (map (\x -> satisfy (==x)) s)

{-
(model
  (define-fun y () Int
    1)
)
-}

