module WLP.RenameFunctions
  ( renameVarInStmt
  , replace
  )
where

import WLP.LogicalExpression
import GCLParser.GCLDatatype

-- | Renames a variable named by the first argument to a variable named by the
-- second argument in the given `Stmt`.
renameVarInStmt :: String -> String -> Stmt -> Stmt
renameVarInStmt o n Skip = Skip
renameVarInStmt o n (Assert e) = Assert $ replaceInExpr o (Var n) e
renameVarInStmt o n (Assume e) = Assume $ replaceInExpr o (Var n) e
renameVarInStmt o n (Assign v e)  = Assign (if o == v then n else v) $ replaceInExpr o (Var n) e
renameVarInStmt o n (AAssign a i v) = AAssign (if o == a then n else a) (replaceInExpr o (Var n) i) (replaceInExpr o (Var n) v)
renameVarInStmt o n (DrefAssign v e)  = DrefAssign (if o == v then n else v) $ replaceInExpr o (Var n) e
renameVarInStmt o n (Seq s1 s2)       = Seq (renameVarInStmt o n s1) (renameVarInStmt o n s2)
renameVarInStmt o n (IfThenElse g t e)  = IfThenElse (replaceInExpr o (Var n) g) (renameVarInStmt o n t) (renameVarInStmt o n e)
renameVarInStmt o n (While g l)         = While (replaceInExpr o (Var n) g) (renameVarInStmt o n l)
renameVarInStmt o n (Block decls s)     = Block (renameInDecl o n <$> decls) (renameVarInStmt o n s)
renameVarInStmt o n (TryCatch err t c)  = error "TryCatch not supported in renameVarInStmt"

-- | Renames a variable named by the first argument to a variable named by the second argument in the given `VarDeclaration`.
renameInDecl :: String -> String -> VarDeclaration -> VarDeclaration
renameInDecl o n (VarDeclaration v t) = flip VarDeclaration t $ if o == v then n else v

-- | Substitutes the variable given by the string with the value given in the expression.
replace :: String -> Expr -> LogicalExpression -> LogicalExpression
replace _   _ x@(Constant _) = x
replace var e x@(Variable v) = if v == var then Expression e else x
replace var e x@(Expression expr) = Expression $ replaceInExpr var e expr
replace var e x@(e1 :/\: e2) = replace var e e1 :/\: replace var e e2
replace var e x@(e1 :\/: e2) = replace var e e1 :\/: replace var e e2
replace var e x@(e1 :=>: e2) = replace var e e1 :=>: replace var e e2
replace var e x@(e1 :<=>: e2) = replace var e e1 :<=>: replace var e e2

-- | Substitutes the variable given by the `String` by the `Expr` given by the
-- second argument in the `Expr` given by the third argument.
replaceInExpr :: String -> Expr -> Expr -> Expr
replaceInExpr var newE x@(Var v) = if var == v then newE else x
replaceInExpr var newE x@(LitI _) = x
replaceInExpr var newE x@(LitB _) = x
replaceInExpr var newE x@(LitNull) = x
replaceInExpr var newE x@(Parens e) = replaceInExpr var newE e
replaceInExpr var newE x@(OpNeg e) = OpNeg $ replaceInExpr var newE e
replaceInExpr var newE x@(BinopExpr op e1 e2) = BinopExpr op (replaceInExpr var newE e1) (replaceInExpr var newE e2)
replaceInExpr var newE x@(Forall v e) = Forall v $ replaceInExpr var newE e
replaceInExpr var newE x@(SizeOf a) = SizeOf $ replaceInExpr var newE a
replaceInExpr var newE x@(RepBy a i v) = let r = replaceInExpr var newE in RepBy (r a) (r i) (r v)
replaceInExpr var newE x@(Cond e1 e2 e3) = error "Cannot yet rename in Cond"
replaceInExpr var newE x@(NewStore e) = error "Cannot yet rename in NewStore"
replaceInExpr var newE x@(Dereference v) = error "Cannot yet rename in Dereference"
replaceInExpr var newE x@(ArrayElem a i)
  = let f = replaceInExpr var newE
    in  ArrayElem (f a) (f i)

