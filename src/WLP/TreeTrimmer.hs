module WLP.TreeTrimmer
  (
    trimIfs
  , TreeTrimmerMode(..)
  )
where

import WLP.ProgramPathTree
import GCLParser.GCLDatatype
import WLP.WLPCalculator
import WLP.LogicalExpression
import Z3.Z3Theorem
import Z3.Z3Runner
import Z3.Z3ModelParser
import WLP.RenameFunctions (replace)

data TreeTrimmerMode
    = Always
    | Never
    | Smart
  deriving (Show, Read)

-- | Whether we should check this branch to eliminate it. Takes the branch and
--   the depth before this branch as parameters.
treeTrimmerHeuristic :: ProgramPathTree -> Int -> Bool
treeTrimmerHeuristic tree depthBefore = branchesInTree tree > 15
-- Old heuristics:
-- (depthBefore < 10) || (leftDepth > depthBefore `div` 2)
-- (depthBefore < 10) || (rightDepth > depthBefore `div` 2)

trimIfs :: TreeTrimmerMode -> ProgramPathTree -> [VarDeclaration] -> Int -> IO ProgramPathTree
trimIfs Never t _ _ = return $! t
trimIfs mode tree variableDeclarations experimentParameter = trimIfs' mode tree [] variableDeclarations experimentParameter

trimIfs' :: TreeTrimmerMode -> ProgramPathTree -> [Stmt] -> [VarDeclaration] -> Int -> IO ProgramPathTree
trimIfs' _ Leaf _ _ _ = return Leaf
trimIfs' m (Node stmt tree) stmts vars experimentParameter = do
  newTree <- trimIfs' m tree (stmt:stmts) vars experimentParameter
  return $ Node stmt newTree

trimIfs' Smart n@(IfNode expr left right) stmtss@(stmt:stmts) vars experimentParameter = do
    let programPath = foldl (flip Seq) stmt stmts
    let leftDepth = pathTreeDepth left
    let rightDepth = pathTreeDepth right
    let depthBefore = length stmtss

    let branchCheck = isBranchSatisfiable programPath expr experimentParameter vars
    
    leftRes <- if treeTrimmerHeuristic left depthBefore
      then branchCheck True
      else return $! Invalid "This branch has been declared satisfiable by our overlord the Heuristic."

    rightRes <- if treeTrimmerHeuristic right depthBefore
      then branchCheck False
      else return $! Invalid "This branch has been declared satisfiable by our overlord the Heuristic."

    doTrimAndCheckInner Smart n stmtss vars experimentParameter (leftRes, rightRes)

trimIfs' Always n@(IfNode expr left right) stmtss@(stmt:stmts) vars experimentParameter = do
    let programPath = foldl (\s1 s2 -> Seq s2 s1) stmt stmts
    let nExpr = OpNeg expr

    leftResult <- isBranchSatisfiable programPath expr experimentParameter vars True
    rightResult <- isBranchSatisfiable programPath expr experimentParameter vars False

    doTrimAndCheckInner Always n stmtss vars experimentParameter (leftResult, rightResult)

-- | Actually does the trimming of the tree, and trims what's left of the
--   children of this tree.
doTrimAndCheckInner :: TreeTrimmerMode -> ProgramPathTree -> [Stmt] -> [VarDeclaration] -> Int -> (SatModel, SatModel) -> IO ProgramPathTree
doTrimAndCheckInner mode (IfNode expr left right) stmtss vars experimentParameter ress =
  do
    let nExpr = OpNeg expr
    case ress of
      -- both branches are satisfiable -> continue with both branches
      (Invalid _, Invalid _) -> do
        left'   <- trimIfs' mode left (Assume expr:stmtss) vars experimentParameter
        right'  <- trimIfs' mode right (Assume nExpr:stmtss) vars experimentParameter
        return $ IfNode expr left' right'
      -- left branch is valid -> continue with left branch
      (Valid, Invalid _) -> do
        left' <- trimIfs' mode left (Assume expr:stmtss) vars experimentParameter
        return $ Node (Assert expr) left'
      -- right branch is valid -> continue with right branch
      (Invalid _, Valid) -> do
        right' <- trimIfs' mode right (Assume nExpr:stmtss) vars experimentParameter
        return $ Node (Assert nExpr) right'
      -- this should never happen
      (Valid, Valid) -> error "Neither branch is possible?"

-- | Checks whether the given branch is satisfiable. Takes the program path so
--   far, the expression belonging to the if-node that we're checking the
--   branches of, the experiment parameter, the variable declarations in the
--   program, and a Bool that is True when we are checking the 'then' branch,
--   and False when we are checking the 'else' side.
isBranchSatisfiable :: Stmt -> Expr -> Int -> [VarDeclaration] -> Bool -> IO SatModel
isBranchSatisfiable pp e ep vars isThen = do
  let e' = if isThen then e else OpNeg e
  let ppWithExpr  = Seq pp $ Assert e'
  let wlpWithExpr = replace "N" (LitI ep) $ programPathToWLP ppWithExpr
  resultAsString <- runZ3 (transformZ3 wlpWithExpr vars) False
  return $! parseSatModel resultAsString

