module WLP.LogicalExpression
  ( LogicalExpression(..)
  , simplify
  , reduceExpressions
  , leSize
  )
where

import GCLParser.GCLDatatype (Expr(..), BinOp(And, Or, Implication), simplifyExpr)

data LogicalExpression
  = Constant Bool
  | Variable String
  | Expression Expr
  | Not LogicalExpression
  | ForAll String LogicalExpression
  | LogicalExpression :/\: LogicalExpression
  | LogicalExpression :\/: LogicalExpression
  | LogicalExpression :=>: LogicalExpression
  | LogicalExpression :<=>: LogicalExpression
  deriving (Eq)

instance Show LogicalExpression where
  show (Constant x)   = show x
  show (Variable s)   = s
  show (Expression e) = "E" ++ show e
  show (Not le)       = "!" ++ show le
  show (ForAll v le)  = "(ForAll " ++ show v ++ " :: " ++ show le ++ ")"

  show (e1 :/\: e2)   = "(" ++ show e1 ++ " /\\ " ++ show e2 ++ ")"
  show (e1 :\/: e2)   = "(" ++ show e1 ++ " \\/ " ++ show e2 ++ ")"
  show (e1 :=>: e2)   = "(" ++ show e1 ++  " => " ++ show e2 ++ ")"
  show (e1 :<=>: e2)  = "(" ++ show e1 ++ " <=> " ++ show e2 ++ ")"

-- | Turns a `LogicalExpression` into a simpler one that has the same
-- semantics, but a simpler syntax. For instance, turns 'True :/\: e' into 'e'
-- and 'True :\/: e' into 'True'.
simplify :: LogicalExpression -> LogicalExpression
-- Conjunction simplification
simplify (e1 :/\: e2) =
  let se1 = simplify e1
      se2 = simplify e2
  in  case (se1, se2) of
        (Constant False, _) -> Constant False
        (_, Constant False) -> Constant False
        (Constant True, le) -> le
        (le, Constant True) -> le
        _ -> if se1 == se2
            then se1
            else se1 :/\: se2

-- Disjunction simplification
simplify (e1 :\/: e2) =
  let se1 = simplify e1
      se2 = simplify e2
  in  case (se1, se2) of
        (Constant True, _) -> Constant True
        (_, Constant True) -> Constant True
        (Constant False, le) -> le
        (le, Constant False) -> le
        _ -> if se1 == se2
            then se1
            else se1 :\/: se2

-- Negation simplification
simplify (Not (Constant True))  = Constant False
simplify (Not (Constant False)) = Constant True
simplify (Not e) = Not $ simplify e

-- Quantification simplification
simplify (ForAll v e) = ForAll v $ simplify e

-- Implication simplification
simplify (Constant False :=>: _)  = Constant True
simplify (e1 :=>: e2) =
  let se1 = simplify e1
      se2 = simplify e2
  in  if se1 == se2
        then se1
        else se1 :=>: se2

-- Expression simplification
simplify (Expression e) = Expression $ simplifyExpr e

-- No simplification
simplify le = le

-- | Reduces Expressions as much as possible, making them part of the
-- `LogicalExpression`s as much as possible. For instance, turns 'Expression
-- (BinopExpr Implication e1 e2)' into 'Expression e1 :=>: Expression e2'.
reduceExpressions :: LogicalExpression -> LogicalExpression
reduceExpressions x@(Constant _)  = x
reduceExpressions x@(Variable _)  = x
reduceExpressions (Not le)        = Not $ reduceExpressions le
reduceExpressions (e1 :/\: e2)    = reduceExpressions e1 :/\:   reduceExpressions e2
reduceExpressions (e1 :\/: e2)    = reduceExpressions e1 :\/:   reduceExpressions e2
reduceExpressions (e1 :=>: e2)    = reduceExpressions e1 :=>:   reduceExpressions e2
reduceExpressions (e1 :<=>: e2)   = reduceExpressions e1 :<=>:  reduceExpressions e2

reduceExpressions (Expression expr) = re' expr
  where
    re' (BinopExpr And e1 e2)         = re' e1 :/\: re' e2
    re' (BinopExpr Or  e1 e2)         = re' e1 :\/: re' e2
    re' (BinopExpr Implication e1 e2) = re' e1 :=>: re' e2
    re' (Parens e)                    = re' e
    re' (Var s)                       = Variable s
    re' (LitB b)                      = Constant b
    re' (OpNeg e)                     = Not $ re' e
    re' (Forall v e)                  = ForAll v $ re' e
    re' e                             = Expression e

leSize :: LogicalExpression -> Int
leSize (Constant _)   = 1
leSize (Variable _)   = 1
leSize (Expression _) = 1
leSize (Not le)       = leSize le
leSize (ForAll _ le)  = leSize le
leSize (le1 :/\: le2) = leSize le1 + leSize le2
leSize (le1 :\/: le2) = leSize le1 + leSize le2
leSize (le1 :=>: le2) = leSize le1 + leSize le2

