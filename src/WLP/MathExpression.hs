module WLP.MathExpression
  ( MathExpression(..)
  , replaceInExpr
  )
where

import Prelude hiding (GT, LT, EQ)

data MathExpression
  = Constant Int
  | Variable String
  | Add MathExpression MathExpression
  | Sub MathExpression MathExpression
  | Mul MathExpression MathExpression
  | Div MathExpression MathExpression
  | GT  MathExpression MathExpression
  | LT  MathExpression MathExpression
  | EQ  MathExpression MathExpression
  | GTE MathExpression MathExpression
  | LTE MathExpression MathExpression

instance Show MathExpression where
  show (Constant n) = show n
  show (Variable s) = s
  show (Add e1 e2)  = "(" ++ show e1 ++ " + "  ++ show e2 ++ ")"
  show (Sub e1 e2)  = "(" ++ show e1 ++ " - "  ++ show e2 ++ ")"
  show (Mul e1 e2)  = "(" ++ show e1 ++ " * "  ++ show e2 ++ ")"
  show (Div e1 e2)  = "(" ++ show e1 ++ " / "  ++ show e2 ++ ")"
  show (GT  e1 e2)  = "(" ++ show e1 ++ " > "  ++ show e2 ++ ")"
  show (LT  e1 e2)  = "(" ++ show e1 ++ " < "  ++ show e2 ++ ")"
  show (EQ  e1 e2)  = "(" ++ show e1 ++ " = "  ++ show e2 ++ ")"
  show (GTE e1 e2)  = "(" ++ show e1 ++ " >= " ++ show e2 ++ ")"
  show (LTE e1 e2)  = "(" ++ show e1 ++ " <= " ++ show e2 ++ ")"

replaceInExpr :: String -> MathExpression -> MathExpression -> MathExpression
replaceInExpr _   _      x@(Constant _) = x
replaceInExpr var newVal x@(Variable s) = if var == s then newVal else x
replaceInExpr var newVal x@(Add e1 e2)  = Add (replaceInExpr var newVal e1) (replaceInExpr var newVal e2)
replaceInExpr var newVal x@(Sub e1 e2)  = Sub (replaceInExpr var newVal e1) (replaceInExpr var newVal e2)
replaceInExpr var newVal x@(Mul e1 e2)  = Mul (replaceInExpr var newVal e1) (replaceInExpr var newVal e2)
replaceInExpr var newVal x@(Div e1 e2)  = Div (replaceInExpr var newVal e1) (replaceInExpr var newVal e2)
replaceInExpr var newVal x@(GT e1 e2)  = GT (replaceInExpr var newVal e1) (replaceInExpr var newVal e2)
replaceInExpr var newVal x@(LT e1 e2)  = LT (replaceInExpr var newVal e1) (replaceInExpr var newVal e2)
replaceInExpr var newVal x@(EQ e1 e2)  = EQ (replaceInExpr var newVal e1) (replaceInExpr var newVal e2)
replaceInExpr var newVal x@(GTE e1 e2)  = GTE (replaceInExpr var newVal e1) (replaceInExpr var newVal e2)
replaceInExpr var newVal x@(LTE e1 e2)  = LTE (replaceInExpr var newVal e1) (replaceInExpr var newVal e2)

