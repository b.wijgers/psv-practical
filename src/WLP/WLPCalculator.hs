module WLP.WLPCalculator
  ( programPathToWLP
  )
where

import Debug.Trace (trace)

import GCLParser.GCLDatatype as GCL

import WLP.LogicalExpression
import WLP.ProgramPath as PP
import WLP.RenameFunctions (replace, renameVarInStmt)

-- | The `LogicalExpression` that this function returns is the WLP for the
-- given `ProgramPath`.
programPathToWLP :: ProgramPath -> LogicalExpression
programPathToWLP = flip pptwlp' (Constant True)
  where
    pptwlp' Skip               le = traceHelp le "Skip" le
    pptwlp' (Assert me)        le =
      let res = Expression me :/\: le
      in  traceHelp res ("Assert " ++ show me) le

    pptwlp' (Assume me)        le =
      let res = Expression me :=>: le
      in  traceHelp res ("Assume " ++ show me) le

    pptwlp' (Assign var e)     le =
      let res = replace var e le
      in  traceHelp res ("Assign " ++ var ++ " " ++ show e) le

    pptwlp' (AAssign arr i v) le  = let e = RepBy (Var arr) i v in pptwlp' (Assign arr e) le
    pptwlp' (DrefAssign _ _)   _  = undefined -- Not mandatory; pointers and stuff
    pptwlp' (Seq s1 s2)        le = (pptwlp' s1 (pptwlp' s2 le))
    pptwlp' (IfThenElse _ _ _) _  = error "Should not calculate WLP for IfThenElse"
    pptwlp' (While _ _)        _  = error "Should not calculate WLP for While"
    pptwlp' (Block decls stmt) le
      = let res = pptwlp' (foldl (\o (VarDeclaration var _) -> renameVarInStmt var (var ++ "$") o) stmt decls) le
        in  traceHelp res ("Block (" ++ show decls ++ ") (" ++ show stmt ++ ")") le

    traceHelp res _ _ = res
    {-
    traceHelp res path le = flip trace res
                          $ "Called pptwlp' with:\n\tpath='" ++ path
                         ++ "'\n\tle='" ++ show le
                         ++ "'\n\tres='" ++ show res ++ "'."
    -}

