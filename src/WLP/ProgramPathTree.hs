module WLP.ProgramPathTree
  ( toProgramPathTree
  , ProgramPathTree(Node, IfNode, Leaf)
  , pathTreeDepth
  , branchesInTree
  )
where

import GCLParser.GCLAlgebra
import GCLParser.GCLDatatype

data ProgramPathTree  = Node Stmt ProgramPathTree
                      | IfNode Expr ProgramPathTree ProgramPathTree
                      | Leaf
                      deriving (Show)

branchesInTree :: ProgramPathTree -> Int
branchesInTree Leaf = 0
branchesInTree (Node _ t) = branchesInTree t
branchesInTree (IfNode _ l r) = 1 + branchesInTree l + branchesInTree r

pathTreeDepth :: ProgramPathTree -> Int
pathTreeDepth Leaf = 0
pathTreeDepth (Node _ t) = 1 + pathTreeDepth t
pathTreeDepth (IfNode _ l r) = 1 + max (pathTreeDepth l) (pathTreeDepth r)

toProgramPathTree :: Program -> Int -> ProgramPathTree
toProgramPathTree = foldProgram toPPTAlgebra

toPPTAlgebra :: ProgramAlgebra PrimitiveType Type VarDeclaration (Int -> (ProgramPathTree, Int)) Expr BinOp (Int -> ProgramPathTree)
toPPTAlgebra =
  ( program
  , variableDeclaration
  , ( ptInt
    , ptBool
    )
  , ( ptype
    , reftype
    , arraytype
    )
  , ( skip
    , assert
    , assume
    , assign
    , aassign
    , drefassign
    , seq
    , ifthenelse
    , while
    , block
    , trycatch
    )
  , ( Var
    , LitI
    , LitB
    , LitNull
    , Parens
    , ArrayElem
    , OpNeg
    , BinopExpr
    , Forall
    , SizeOf
    , RepBy
    , Cond
    , NewStore
    , Dereference
    )
  , ( And
    , Or
    , Implication
    , LessThan
    , LessThanEqual
    , GreaterThan
    , GreaterThanEqual
    , Equal
    , Minus
    , Plus
    , Multiply
    , Divide
    , Alias
    )
  )
  where
    program _ _ _ stmt  = fst . stmt
    variableDeclaration = undefined
    ptInt               = undefined
    ptBool              = undefined
    ptype               = undefined
    reftype             = undefined
    arraytype           = undefined
    skip                = \i -> (Node Skip Leaf, i-1)
    assert me           = \i -> (Node (Assert me) Leaf, i-1)
    assume me           = \i -> (Node (Assume me) Leaf, i-1)
    assign var e        = \i -> (Node (Assign var e) Leaf, i-1)
    aassign var i' v    = \i -> (Node (AAssign var i' v) Leaf, i-1)
    drefassign var v    = \i -> (Node (DrefAssign var v) Leaf, i-1)
    ifthenelse g s1 s2  = \i -> (IfNode g (fst $ s1 i) (fst $ s2 i), i-1)
    while g s           = \i -> let (stmt, lenLeft) = s i
                                in  if lenLeft <= 0
                                      then (Leaf, -1)
                                      else case addToEnd i stmt (fst (while g s lenLeft)) of
                            Just s' -> (IfNode g s' Leaf, lenLeft)
                            _       -> (Leaf, lenLeft)
    block decls s       = \i -> s i
    trycatch            = undefined -- We don't do that here

    seq s1 s2 = \i ->
      let (n1, ll1) = s1 i
          (n2, ll2) = s2 ll1
      in  (seq' i n1 n2, ll2)
    seq' i (Node p1 Leaf) (Node p2 n2)         = Node p1 $ Node p2 n2
    seq' i (Node p1 n1) n2@(Node _ _)          = case addToEnd i n1 n2 of
                                                Just n' -> Node p1 n'
                                                _       -> Leaf

    seq' i (IfNode g pt pe) n                  = case addToEnd i pt n of
                                                Just t' -> case addToEnd i pe n of
                                                  Just e' -> IfNode g t' e'
                                                  _       -> t'
                                                _       -> case addToEnd i pe n of
                                                  Just e' -> e'
                                                  _       -> Leaf
    seq' i (Node s Leaf) n@(IfNode _ _ _)      = Node s n

    seq' i s1 s2 = error $ "Non exhaustive patterns in seq, file WLP/ProgramPathTree, line ~110.\n"
                        ++ "No pattern exists for following arguments:\n\t"
                        ++ show s1 ++ "\n\t" ++ show s2

    addToEnd :: Int -> ProgramPathTree -> ProgramPathTree -> Maybe ProgramPathTree
    addToEnd = ate' 0

    ate' c m Leaf n                 = if c >= m then Nothing else Just n
    ate' c m (Node s Leaf) n        = if c >= m then Nothing else Just $ Node s n
    ate' c m (Node s n1) n2         = if c >= m then Nothing else
                                      case ate' (c + 1) m n1 n2 of
                                        Just n' -> Just $ Node s n'
                                        _       -> Nothing
    ate' c m (IfNode g n1 n2) n3    = if c >= m then Nothing else
                                      let n1' = ate' (c+1) m n1 n3
                                          n2' = ate' (c+1) m n2 n3
                                      in  case n1' of
                                        Just n1'' -> case n2' of
                                          Just n2'' -> Just $ IfNode g n1'' n2''
                                          _         -> Just $ Node (Assume g) n1''
                                        _         -> case n2' of
                                          Just n2'' -> Just $ Node (Assume (OpNeg g)) n2''
                                          _         -> Nothing
    ate' c m n1 n2                  = error $ "No pattern for arguments:\n\t" ++ show n1 ++ "\n\t" ++ show n2

