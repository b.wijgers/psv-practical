module WLP.ProgramPath
  ( ProgramPath
  , getProgramPaths
  , PathLength
  , normalizeProgramPath
  )
where

import GCLParser.GCLAlgebra
import GCLParser.GCLDatatype

import WLP.ProgramPathTree

-- Program paths and stuff
-- The program paths are all just a single statement (using Seq), so we use that.
type ProgramPath = Stmt
type PathLength = Int

-- | Gets all program paths from the program with the given max path length.
-- Note that some statements might be expanded to multiple statements, and the
-- path length is based on the length of the expanded statements. Additionally,
-- some statements might be removed (like Skip).
getProgramPaths :: PathLength -> ProgramPathTree -> [ProgramPath]
getProgramPaths = gpp' 0 Skip
  where
    -- Current length, previous statement max length, tree. Returns the paths in the tree up to the specified max length.
    gpp' :: PathLength -> Stmt -> PathLength -> ProgramPathTree -> [ProgramPath]
    gpp' _ s _ Leaf = [s]
    gpp' c _ m t    =
      if c > m then [] else case t of
        Leaf          -> error "Should not have a leaf here..."
        Node s t'     -> Seq s <$> (gpp' (c + 1) s m t')
        IfNode g t e  ->  ((Seq (Assume g))         <$> (gpp' (c+1) (Assume g) m t))
                      ++  ((Seq (Assume $ OpNeg g)) <$> (gpp' (c+1) (Assume $ OpNeg g) m e))

-- Note that this path should no longer contain ifs and whiles, but should be
-- the unrolled variants of those statements.
pathLength :: ProgramPath -> PathLength
pathLength (Seq s1 s2) = pathLength s1 + pathLength s2
pathLength _           = 1

-- | Unrolls a while-statement into all possible program paths. The while-loop
-- is defined by the given expression (the guard expression) and the body. The
-- list of all program paths is infinite, and therefore the second argument
-- (`PathLength`) is used to limit the allowed length of the path. The body is
-- the third argument (`PathLength` -> [`ProgramPath`]). This comes from the type of
-- the enclosing `programPathAlgebra`.
-- Returns a [`Stmt`], which is a list of the program paths that can be
-- traversed within this while-loop.
unrollWhile :: Expr -> PathLength -> (PathLength -> [ProgramPath]) -> [ProgramPath]
unrollWhile g ml s = uw' g ml s []
  where
    -- This function takes the guard expression, the max path length, and the
    -- statement, as well as the list of previously found paths, and returns
    -- the previously found paths prepended by a single iteration of the loop.
    -- Note that calling this function with no paths returns the path where the
    -- loop is never entered.
    uw' g ml s []
      = let xs = [Seq (Assume (OpNeg g)) Skip] in xs `seq` (xs ++ uw' g ml s xs)

    uw' g ml s prev
      = let xs =  [ Seq (Assume g) (Seq s' p)
                  | s' <- s ml
                  , p <- prev
                  , pathLength p + pathLength s' < ml
                  ]
        in  seq xs $ case xs of
          [] -> []
          xs -> (xs ++ uw' g ml s xs)

-- | Normalizes a program path. This changes all instances of `Seq (Seq s1 s2)
-- s3` into `Seq s1 (Seq s2 s3)`, which should avoid a bunch of error later on.
normalizeProgramPath :: ProgramPath -> ProgramPath
normalizeProgramPath (Seq (Seq s1 s2) s3)
  = let n1 = normalizeProgramPath s1
        n2 = normalizeProgramPath s2
        n3 = normalizeProgramPath s3
    in  Seq n1 (Seq n2 n3)
normalizeProgramPath x = x

