# Program verification tool for PSV

This verification tool is a bounded backwards symbolic execution verifier that
works on GCL programs. It can verify program paths up to a certain depth, given
as a command line argument. There is also an experiment parameter for the
benchmarks, but this parameter is not used for normal programs.

# Building the program

Building the program should be as simple as running `cabal build`. If it is
not, please message me (b.wijgers@students.uu.nl).

# Running the program

This can be done either with a pre-built executable (see Building the program)
or using cabal. When using cabal, use `cabal run -- [args]`. Otherwise, use
`<executable-name> [args]` as normal.

# Benchmarking

The program is mainly meant as an experimentation tool, and as such, will run
benchmarks by default. If you simply wish to verify a program, run it with the
-v option.

# Command-line options

Running the program with `-h` will give a help message as well. A more verbose
version, including example usages, is given here:
  * `-i [path]`: the path to the file containing the program that will be
                  verified.
  * `-k [int]`: the maximum length of all paths that will be verified. All
                paths longer than this are considered valid!
  * `-N [int]`: the experiment parameter for the benchmarks. Will set all
                variables named N to this exact integer value. Note that this
                has a default value!
  * `-t [Always/Never/Smart]`: the tree-trimming policy. Will attempt to reduce
                            the amount of program paths by checking whether
                            some branches will always/never be taken. At the
                            moment, Never seems to be the most optimal in
                            (almost) all cases.
  * `-d`: run the program in debug/verbose mode. Adds a lot of output useful when
      debugging the program.
  * `-v`: run the program in verification mode. Without this, the tool will
      automatically run benchmarks.

Note that any usage that does not use -v will run on a set of values for -k, -t
and -N, and these values will be ignored in that case.

Example usage:
  `cabal run -- -i input.gcl -k 100 -t Never -v`: runs the program on
  "input.gcl" with a maximum path length of 100 and without ever trying to
  eliminate branches. Only verifies the program, without creating benchmark
  data.

  `cabal run -- -i program.gcl`: runs the program on "program.gcl" in benchmark
                                  mode. Uses a maximum path length of 20, the
                                  default. Always tries to eliminate all
                                  branches, which is currently also the
                                  default. Does not run in debug mode.

